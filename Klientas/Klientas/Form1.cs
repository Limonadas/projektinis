using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Drawing.Imaging;

namespace Klientas
{
    public partial class Form1 : Form
    {
        Graphics graphics;
        Bitmap picture = new Bitmap(464, 520);
        Point startLocation = new Point();
        Pen myPen;
        int penWidth = 1;
        Color myColor = Color.Black;
        bool isDrawing = false;
        bool isDrawer = false;
        bool playing;
        string userName;
        private static Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        ListBox playerBox = new ListBox()
        {
            FormattingEnabled = true,
            Location = new Point(12, 45),
            Size = new Size(100, 368),
            TabIndex = 5
        };
        ListBox chatBox = new ListBox()
        {
            FormattingEnabled = true,
            Location = new Point(145, 45),
            Size = new Size(150, 368),
            TabIndex = 5
        };

        public Form1()
        {
            InitializeComponent();
        }

        private void LoopConnect()
        {
            while (clientSocket.Connected == false)
            {
                string serverIp = File.ReadAllText("ip.txt");
                try
                {
                    clientSocket.Connect(serverIp, 100);
                }
                catch (SocketException)
                {

                }
            }
            byte[] receivedBuffer = new byte[1024];
            int received = clientSocket.Receive(receivedBuffer);
            byte[] data = new byte[received];
            Array.Copy(receivedBuffer, data, received);
            userName = Encoding.ASCII.GetString(data);
            userLabel.Text = userName;
            playerBox.Items.Add(userName + " 0");
            playing = true;
            Thread t = new Thread(GetData);
            t.Start();
        }

        void GetData()
        {
            BeginInvoke((Action)delegate ()
            {
                Controls.Add(chatBox);
                Controls.Add(playerBox);
            });
            while (playing)
            {
                byte[] receivedBuffer = new byte[102400];
                int received = clientSocket.Receive(receivedBuffer);
                byte[] data = new byte[received];
                Array.Copy(receivedBuffer, data, received);
                string message = Encoding.ASCII.GetString(data);
                if (message.Length < 3)
                {
                    continue;
                }
                if (message.Substring(0, 3) == "usr")
                {
                    BeginInvoke((Action)delegate ()
                        {
                            chatBox.Items.Add(message.Substring(3, message.Length - 3));
                        });
                }
                else if (message.Substring(0, 3) == "tm$")
                {
                    try
                    {
                        BeginInvoke((Action)delegate ()
                        {
                            timeLabel.Text = message.Substring(3, message.Length - 3);
                        });
                    }
                    catch (Exception)
                    {
                        Environment.Exit(0);
                    }
                }
                else if (message.Substring(0, 3) == "wrd")
                {
                    BeginInvoke((Action)delegate ()
                    {
                        if (isDrawer)
                        {
                            wordLabel.Text = message.Substring(3, message.Length - 3);
                        }
                        else
                        {
                            wordLabel.Text = "";
                            foreach (char letter in message.Substring(3, message.Length - 3))
                            {
                                if (letter == ' ')
                                    wordLabel.Text += " ";
                                else
                                    wordLabel.Text += "*";
                            }
                        }
                    });
                }
                else if (message.Substring(0, 3) == "plj")
                {
                    BeginInvoke((Action)delegate ()
                    {
                        playerBox.Items.Add(message.Substring(3, message.Length - 3));
                    });
                }
                else if (message.Substring(0, 3) == "ydr")
                {
                    BeginInvoke((Action)delegate ()
                    {
                        isDrawer = true;
                        pictureBox1.BackColor = Color.White;
                    });
                }
                else if (message.Substring(0, 3) == "ndr")
                {
                    BeginInvoke((Action)delegate ()
                    {
                        isDrawer = false;
                    });
                }
                else if (message.Substring(0, 3) == "upd")
                {
                    BeginInvoke((Action)delegate ()
                    {
                        playerBox.Items.Clear();
                    });
                }
                else if (message != null && !isDrawer)
                {
                    using (var aa = new MemoryStream(receivedBuffer))
                    {
                        aa.Position = 0;
                        Bitmap bmp = new Bitmap(aa);
                        pictureBox1.Image = bmp;
                    }
                }
            }
        }

        void LoadDraw()
        {
            graphics = Graphics.FromImage(picture);
            graphics.Clear(Color.White);
            colorButton.BackColor = myColor;
            UpdatePen();
            UpdatePicture();
        }

        void UpdatePicture()
        {
            graphics = Graphics.FromImage(picture);
            pictureBox1.Image = picture;
        }

        void UpdatePen()
        {
            myPen = new Pen(myColor, penWidth);
        }

        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
        
        private void penUpDown_ValueChanged_1(object sender, EventArgs e)
        {
            penWidth = (int)penUpDown.Value;
            UpdatePen();
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            string request = userName + " " + sendBox.Text;
            byte[] buffer = Encoding.ASCII.GetBytes(request);
            clientSocket.Send(buffer);
            sendBox.Text = "";
        }

        private void colorButton_Click_1(object sender, EventArgs e)
        {
            var colorPicker = new ColorDialog();
            if (colorPicker.ShowDialog() == DialogResult.OK)
            {
                myColor = colorPicker.Color;
                colorButton.BackColor = myColor;
                UpdatePen();
            }
        }

        private void penUpDown_ValueChanged(object sender, EventArgs e)
        {
            penWidth = (int)penUpDown.Value;
            UpdatePen();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (isDrawer == true)
            {
                isDrawing = true;
                startLocation = e.Location;
            }
        }

        private void pictureBox1_MouseUp_1(object sender, MouseEventArgs e)
        {
            if (isDrawer)
            {
                clientSocket.Send(ImageToByteArray(picture));
                isDrawing = false;
            }
        }

        public byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDrawing && isDrawer)
            {
                if (lineButton.Checked)
                {
                    graphics.DrawRectangle(myPen, new Rectangle(startLocation, new Size(penWidth, penWidth)));
                    startLocation = e.Location;
                }
                else if (eraserButton.Checked)
                {
                    Pen eraserPen = new Pen(Color.White, penWidth);
                    graphics.DrawRectangle(eraserPen, new Rectangle(startLocation, new Size(penWidth, penWidth)));
                    startLocation = e.Location;
                }
                UpdatePicture();
            }
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            LoopConnect();
            LoadDraw();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            playing = false;
            string request = userName + "closing$";
            byte[] buffer = Encoding.ASCII.GetBytes(request);
            clientSocket.Send(buffer);
        }
    }
}

