namespace Klientas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendBox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.colorButton = new System.Windows.Forms.Button();
            this.penUpDown = new System.Windows.Forms.NumericUpDown();
            this.lineButton = new System.Windows.Forms.RadioButton();
            this.eraserButton = new System.Windows.Forms.RadioButton();
            this.wordLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.userLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.penUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // sendBox
            // 
            this.sendBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendBox.Location = new System.Drawing.Point(12, 552);
            this.sendBox.Name = "sendBox";
            this.sendBox.Size = new System.Drawing.Size(262, 31);
            this.sendBox.TabIndex = 0;
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(280, 552);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(85, 30);
            this.sendButton.TabIndex = 1;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(372, 61);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(464, 520);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp_1);
            // 
            // colorButton
            // 
            this.colorButton.Location = new System.Drawing.Point(730, 12);
            this.colorButton.Name = "colorButton";
            this.colorButton.Size = new System.Drawing.Size(106, 43);
            this.colorButton.TabIndex = 3;
            this.colorButton.UseVisualStyleBackColor = true;
            this.colorButton.Click += new System.EventHandler(this.colorButton_Click_1);
            // 
            // penUpDown
            // 
            this.penUpDown.Location = new System.Drawing.Point(570, 23);
            this.penUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.penUpDown.Name = "penUpDown";
            this.penUpDown.Size = new System.Drawing.Size(42, 20);
            this.penUpDown.TabIndex = 4;
            this.penUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.penUpDown.ValueChanged += new System.EventHandler(this.penUpDown_ValueChanged);
            // 
            // lineButton
            // 
            this.lineButton.AutoSize = true;
            this.lineButton.Checked = true;
            this.lineButton.Location = new System.Drawing.Point(618, 23);
            this.lineButton.Name = "lineButton";
            this.lineButton.Size = new System.Drawing.Size(45, 17);
            this.lineButton.TabIndex = 5;
            this.lineButton.TabStop = true;
            this.lineButton.Text = "Line";
            this.lineButton.UseVisualStyleBackColor = true;
            // 
            // eraserButton
            // 
            this.eraserButton.AutoSize = true;
            this.eraserButton.Location = new System.Drawing.Point(669, 23);
            this.eraserButton.Name = "eraserButton";
            this.eraserButton.Size = new System.Drawing.Size(55, 17);
            this.eraserButton.TabIndex = 6;
            this.eraserButton.TabStop = true;
            this.eraserButton.Text = "Eraser";
            this.eraserButton.UseVisualStyleBackColor = true;
            // 
            // wordLabel
            // 
            this.wordLabel.AutoSize = true;
            this.wordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wordLabel.Location = new System.Drawing.Point(425, 18);
            this.wordLabel.Name = "wordLabel";
            this.wordLabel.Size = new System.Drawing.Size(58, 25);
            this.wordLabel.TabIndex = 7;
            this.wordLabel.Text = "word";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.Location = new System.Drawing.Point(367, 18);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(52, 25);
            this.timeLabel.TabIndex = 8;
            this.timeLabel.Text = "time";
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userLabel.Location = new System.Drawing.Point(113, -2);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(111, 42);
            this.userLabel.TabIndex = 9;
            this.userLabel.Text = "name";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 595);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.wordLabel);
            this.Controls.Add(this.eraserButton);
            this.Controls.Add(this.lineButton);
            this.Controls.Add(this.penUpDown);
            this.Controls.Add(this.colorButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.sendBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "DrawItBootleg";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.penUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox sendBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button colorButton;
        private System.Windows.Forms.NumericUpDown penUpDown;
        private System.Windows.Forms.RadioButton lineButton;
        private System.Windows.Forms.RadioButton eraserButton;
        private System.Windows.Forms.Label wordLabel;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label userLabel;
    }
}

