using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {

        class User
        {
            public Socket socket;
            public string name;
            public int taskai = 0;
            public bool connected = true;
            public User(Socket socketGet, string nameGet)
            {
                socket = socketGet;
                name = nameGet;
            }
        }
        private static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //serveris

        private static List<Socket> clientSockets = new List<Socket>();

        private static byte[] buffer = new byte[102400]; // informacija kuria sius

        static int randomWordInt = -1;
        static int guessedRight = 0;
        static int drawer = 0;
        static bool intermission = false;

        static List<User> userList = new List<User>();

        static List<string> words = new List<string> { "obuolys", "ziogas", "kaladele", "pedalas", "123!!!!"};

        static bool pirmasVartotojas = false;

        static Thread t = new Thread(game);

        static void Main(string[] args)
        {
            SetupServer();
            Console.ReadKey();
        }
        private static void SetupServer()
        {
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, 100));
            serverSocket.Listen(1);
            serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            Console.WriteLine("Serveris Paruostas");
        }

        private static void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = serverSocket.EndAccept(AR);
            User user = new User(socket, (userList.Count + 1).ToString() + " Vartotojas");
            Console.WriteLine("Connected : " + user.name);
            byte[] sendData = Encoding.ASCII.GetBytes(user.name);
            user.socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), user.socket);
            for (int i = 0; i < userList.Count; i++)
            {
                Thread.Sleep(100);
                sendData = Encoding.ASCII.GetBytes("plj" + userList[i].name + " " + userList[i].taskai.ToString());
                user.socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), user.socket);
            }
            for (int i = 0; i < userList.Count; i++)
            {
                sendData = Encoding.ASCII.GetBytes("plj" + user.name + " " + user.taskai.ToString());
                userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
            }
            userList.Add(user);
            socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

            if (!pirmasVartotojas)
            {
                t.Start();
                pirmasVartotojas = true;
            }
            else
            {
                Thread.Sleep(500);
                sendData = Encoding.ASCII.GetBytes("wrd" + words[randomWordInt]);
                user.socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), user.socket);
            }
        }

        static void game()
        {
            while (true)
            {
                NewRoundSetup();
                byte[] sendData = Encoding.ASCII.GetBytes("wrd" + words[randomWordInt]);
                for (int i = 0; i < userList.Count; i++)
                {   
                    if(userList[i].connected)
                        userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                }
                for (int time = 30; time >= 0; time--)
                {
                    Thread.Sleep(1000);
                    sendData = Encoding.ASCII.GetBytes("tm$" + time.ToString());
                    for (int i = 0; i < userList.Count; i++)
                    {   
                        if(userList[i].connected)
                            userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                    }
                    if (guessedRight == userList.Count-1 && userList.Count > 1)
                    {
                        guessedRight = 0;
                        goto roundEnd;
                    }
                }
                roundEnd: RoundEnd();
            }
        }

        private static void NewRoundSetup()
        {
            Thread.Sleep(1000);
            Random rnd = new Random();
            byte[] sendData = Encoding.ASCII.GetBytes("ydr");
            if (drawer >= userList.Count)
            {
                drawer = 0;
            }
            if(userList[drawer].connected)
                userList[drawer].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[drawer].socket);
            if(randomWordInt != -1)
                words.RemoveAt(randomWordInt);
            if (words.Count() - 1 >= 0)
                randomWordInt = rnd.Next(words.Count() - 1);
            else
                throw new NotImplementedException();
            intermission = false;
        }

        private static void RoundEnd()
        {
            byte[] sendData;
            intermission = true;
            for (int i = 0; i < userList.Count; i++)
            {
                sendData = Encoding.ASCII.GetBytes("upd");
                if (!userList[i].connected)
                    continue;
                userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                for (int h = 0; h < userList.Count; h++)
                {
                    if (!userList[h].connected)
                        continue;
                    Thread.Sleep(100);
                    sendData = Encoding.ASCII.GetBytes("plj" + userList[h].name + " " + userList[h].taskai.ToString());
                    userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                }
            }
            Thread.Sleep(100);
            sendData = Encoding.ASCII.GetBytes("ndr");
            if (userList[drawer].connected)
                userList[drawer].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[drawer].socket);
            drawer++;
        }

        private static void ReceiveCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            int received;
            try
            {
                received = socket.EndReceive(AR);
                byte[] dataBuffer = new byte[received];
                Array.Copy(buffer, dataBuffer, received);
                byte[] sendData;

                string text = Encoding.ASCII.GetString(dataBuffer);
                if (text.Contains("Vartotojas"))
                {
                    Console.WriteLine(text);
                    if(text.Contains("closing$"))
                    {
                        userList[text[0] - '1'].connected = false;
                        userList[text[0] - '1'].socket.Shutdown(SocketShutdown.Both);
                    }
                    else if (text.Substring(13, text.Length - 13) == words[randomWordInt] && !intermission && !text.Contains(userList[drawer].name))
                    {
                        sendData = Encoding.ASCII.GetBytes("usr" + text[0] + " vartotojas atspejo");
                        guessedRight++;
                        userList[text[0]-'1'].taskai++;
                        for (int i = 0; i < userList.Count; i++)
                        {   if(userList[i].connected)
                                 userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                        }
                    }
                    else
                    {
                        sendData = Encoding.ASCII.GetBytes("usr" + text);
                        for (int i = 0; i < userList.Count; i++)
                        {
                            if (userList[i].connected)
                                userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                        }
                    }
                }
                else
                {
                    sendData = dataBuffer;
                    for (int i = 0; i < userList.Count; i++)
                    {
                        if (drawer == i || !userList[i].connected)
                        {
                            continue;
                        }
                        userList[i].socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), userList[i].socket);
                    }
                }
            }
            catch (SocketException)
            {

            }
            try
            {
                socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            }
            catch{ }
        }

        private static void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }
    }
}